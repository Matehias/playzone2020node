const mongoose = require("mongoose");

let humiditeAir = mongoose.Schema(
    // Attributs de l'Objet :
    {
        box : {type : String},
        value : {type : Number},
        date : {type:  Date}
    },
// Collection à utiliser
    {colllection : "humiditeairs"}
);

// Exporter le schéma (modèle):
module.exports = mongoose.model("humiditeAir", humiditeAir);
