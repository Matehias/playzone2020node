const mongoose = require("mongoose");

let consigneTemp = mongoose.Schema(
    // Attributs de l'Objet :
    {
        value : {type : Number},
    },
// Collection à utiliser
    {colllection : "consignetemps"}
);

// Exporter le schéma (modèle):
module.exports = mongoose.model("consigneTemp", consigneTemp);
