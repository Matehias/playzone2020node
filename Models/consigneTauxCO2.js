const mongoose = require("mongoose");

let consigneTauxCO2 = mongoose.Schema(
    // Attributs de l'Objet :
    {
        value : {type : Number},
    },
// Collection à utiliser
    {colllection : "consignetauxco2s"}
);

// Exporter le schéma (modèle):
module.exports = mongoose.model("consigneTauxCO2", consigneTauxCO2);
