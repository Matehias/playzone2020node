const mongoose = require("mongoose");

let consigneHumidAir = mongoose.Schema(
    // Attributs de l'Objet :
    {
        value : {type : Number},
    },
// Collection à utiliser
    {colllection : "consignehumidairs"}
);

// Exporter le schéma (modèle):
module.exports = mongoose.model("consigneHumidAir", consigneHumidAir);
