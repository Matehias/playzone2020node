const mongoose = require("mongoose");

let tempAir = mongoose.Schema(
    // Attributs de l'Objet :
    {
        box : {type : String},
        value : {type : Number},
        date : {type:  Date}
    },
// Collection à utiliser
    {colllection : "tempairs"}
);

// Exporter le schéma (modèle):
module.exports = mongoose.model("tempAir", tempAir);

