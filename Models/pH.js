const mongoose = require("mongoose");

let pH = mongoose.Schema(
    // Attributs de l'Objet :
    {
        box : {type : String},
        value : {type : Number},
        date : {type:  Date}
    },
// Collection à utiliser
    {colllection : "phs"}
);

// Exporter le schéma (modèle):
module.exports = mongoose.model("pH", pH);
