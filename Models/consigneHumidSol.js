const mongoose = require("mongoose");

let consigneHumiSol = mongoose.Schema(
    // Attributs de l'Objet :
    {
        value : {type : Number},
    },
// Collection à utiliser
    {colllection : "consignehumisols"}
);

// Exporter le schéma (modèle):
module.exports = mongoose.model("consigneHumiSol", consigneHumiSol);
