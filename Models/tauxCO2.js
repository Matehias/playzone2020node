const mongoose = require("mongoose");

let tauxCO2 = mongoose.Schema(
    // Attributs de l'Objet :
    {
        box : {type : String},
        value : {type : Number},
        date : {type:  Date}
    },
// Collection à utiliser
    {colllection : "tauxco2s"}
);

// Exporter le schéma (modèle):
module.exports = mongoose.model("tauxCO2", tauxCO2);
