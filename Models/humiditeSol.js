const mongoose = require("mongoose");

let humiditeSol = mongoose.Schema(
    // Attributs de l'Objet :
    {
        plante : {type : String},
        value : {type : Number},
        date : {type:  Date}
    },
// Collection à utiliser
    {colllection : "humiditesols"}
);

// Exporter le schéma (modèle):
module.exports = mongoose.model("humiditeSol", humiditeSol);

