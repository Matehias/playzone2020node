const  HumiditeSol = require('../Models/humiditeSol');
const ConsigneHumiSol = require('../Models/consigneHumidSol');


const getAllHumidSol = (req, res) =>{

    HumiditeSol.find({}, "-_id -__v")
        .then(data => res.status(200).json(data))
        .catch(error => res.status(400).json(error));

};

exports.getAllHumidSol = getAllHumidSol;

const postHumidSol = (plante, value, res, SocketIO) => {


    try {
        // création d'une variable (objet) de type schéma Mongoose HumiditeSol à partir du query (dans l'URL) de la requete
        let humiditeSol = new HumiditeSol();
        humiditeSol.plante=plante;
        humiditeSol.value=value;
        humiditeSol.date = Date.now();
        console.log(humiditeSol);

        // Sauvegarde en db de l'objet contenu dans humiditeSol
        humiditeSol.save();
        humisolEmitAll(SocketIO);
        humisolEmitLast(SocketIO);
        // Status http + affichage d'un message :
        // res.status(200).json("Enregistrement en db réussi !");
    } catch (e) {
        console.log("L'enregistrement du message a échoué");
        res.status(500).json(e);

    }
};

exports.posHumidSol = postHumidSol;

//Fonction d'emission d'une nouvelle humidité socketio :
async function humisolEmitLast(socketIO) {
    console.log("emet une nouvelle humidite sol");
    var newHumisol = await HumiditeSol.findOne({}, '-_id -__v', {sort: {'_id': -1}});

    if (newHumisol.length !== null) {
        socketIO.emit("HumiSolHubNew", newHumisol);
    }
}

exports.humisolEmitLast = humisolEmitLast;


//Fonction d'emission de toutes les humisSol socketio :
async function humisolEmitAll(socketIO) {
    console.log("emet toutes les humidite sol");
    var humisolAll = await HumiditeSol.find({}, '-_id -__v', {sort : {'date' : 1}});

    if (humisolAll.length !== null) {
        socketIO.emit("HumiSolHub", humisolAll);
    }
}

exports.humisolEmitAll = humisolEmitAll;

// Fonction pour récup et envoyer la consigne
const consigneHumiSol = (req, res, SocketIO) =>{


    ConsigneHumiSol.deleteMany().then(
        ()=>{
            let ConsigneHumisSol = new ConsigneHumiSol(req.query);
            // console.log(ConsigneHumisSol);
            ConsigneHumisSol.save();
            consigneHumisSolEmitAll(SocketIO);
            res.status(200).json();
        }).catch((error) => {
        res.status(200).json();
    })



};
exports.consigneHumiSol=consigneHumiSol;

//Fonction d'emission de l'humidité de consigne :
async function consigneHumisSolEmitAll(socketIO) {

    // console.log("emet l'humidité de consigne");
    // Varibale de transit afin d'attendre la fin du delete many/save
    var transit = await ConsigneHumiSol.findOne({});

    await ConsigneHumiSol.findOne({}).then(
        (consigneHumisSol) => {
            if (consigneHumisSol.length !== null)
            {
                socketIO.emit("consigneHumisSolHub", consigneHumisSol);
            }
        }
    )



}
exports.consigneHumisSolEmitAll =consigneHumisSolEmitAll;
