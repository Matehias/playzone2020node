const HumidAir = require('../Models/humiditeAir');
const ConsigneHumidAir = require('../Models/consigneHumidAir');

const getAllHumidAir = (req, res) =>{

    HumidAir.find({}, "-_id -__v")
        .then(data => res.status(200).json(data))
        .catch(error => res.status(400).json(error));

};

exports.getAllHumidAir = getAllHumidAir;

const postHumidAir = (box, value, res, SocketIO) => {


    try {
        // création d'une variable (objet) de type schéma Mongoose HumidAir à partir du query (dans l'URL) de la requete
        let humidAir = new HumidAir();
        humidAir.box=box;
        humidAir.value=value;
        humidAir.date = Date.now();
        console.log(humidAir);

        // Sauvegarde en db de l'objet contenu dans HumidAir
        humidAir.save();
        humidAirEmitAll(SocketIO);
        humidAirlEmitLast(SocketIO);
        // Status http + affichage d'un message :
        // res.status(200).json("Enregistrement en db réussi !");
    } catch (e) {
        console.log("L'enregistrement du message a échoué");
        res.status(500).json(e);

    }
};

exports.postHumidAir = postHumidAir;

//Fonction d'emission d'une nouvelle humidité air socketio :
async function humidAirlEmitLast(socketIO) {
    console.log("emet une nouvelle humidité air");
    var newHumidAir = await HumidAir.findOne({}, '-_id -__v', {sort: {'_id': -1}});

    if (newHumidAir.length !== null) {
        socketIO.emit("humidAirHubNew", newHumidAir);
    }
}

exports.humidAirlEmitLast = humidAirlEmitLast;


//Fonction d'emission de toutes les humidité air socketio :
async function humidAirEmitAll(socketIO) {
    console.log("emet toutes les humidité air");
    var humidAirlAll = await HumidAir.find({}, '-_id -__v', {sort : {'date' : 1}});

    if (humidAirlAll.length !== null) {
        socketIO.emit("humidAirlAllHub", humidAirlAll);
    }
}

exports.humidAirEmitAll = humidAirEmitAll;

// Fonction pour récup et envoyer la consigne
const consigneHumidAir = (req, res, SocketIO) =>{


    ConsigneHumidAir.deleteMany().then(
        ()=>{
            let consigneHumidAir = new ConsigneHumidAir(req.query);
            // console.log(consigneHumidAir);
            consigneHumidAir.save();
            consigneHumidAirEmitAll(SocketIO);
            res.status(200).json();
        }).catch((error) => {
        res.status(200).json();
    })



};
exports.consigneHumidAir=consigneHumidAir;

//Fonction d'emission de la humidité de consigne :
async function consigneHumidAirEmitAll(socketIO) {

    // console.log("emet l'humidité air de consigne");
    // Varibale de transit afinn d'attendre la fin deu delete many/save
    var transit = await ConsigneHumidAir.findOne({});

    let consigneHumidAir = await ConsigneHumidAir.findOne({});


    if (consigneHumidAir.length !== null) {
        socketIO.emit("consigneHumidAirHub", consigneHumidAir);
    }
}
exports.consigneHumidAirEmitAll =consigneHumidAirEmitAll;

