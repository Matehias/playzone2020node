const TauxCO2 = require('../Models/tauxCO2');
const ConsigneTauxCO2 = require('../Models/consigneTauxCO2');

const getAllTauxCO2 = (req, res) =>{

    TauxCO2.find({}, "-_id -__v")
        .then(data => res.status(200).json(data))
        .catch(error => res.status(400).json(error));

};

exports.getAllTauxCO2 = getAllTauxCO2;

const postTauxCO2 = (box, value, res, SocketIO) => {


    try {
        // création d'une variable (objet) de type schéma Mongoose TempAir à partir du query (dans l'URL) de la requete
        let tauxCO2 = new TauxCO2();
        tauxCO2.box =box;
        tauxCO2.value = value;
        tauxCO2.date = Date.now();
        console.log(tauxCO2);

        // Sauvegarde en db de l'objet contenu dans tempAir
        tauxCO2.save();
        tauxCO2EmitAll(SocketIO);
        tauxCO2lEmitLast(SocketIO);
        // Status http + affichage d'un message :
        // res.status(200).json("Enregistrement en db réussi !");
    } catch (e) {
        console.log("L'enregistrement du message a échoué");
        res.status(500).json(e);

    }
};

exports.postTauxCO2 = postTauxCO2;

//Fonction d'emission d'une nouvelle temp air socketio :
async function tauxCO2lEmitLast(socketIO) {
    console.log("emet un nouveau taux de CO2");
    var newTauxCO2 = await TauxCO2.findOne({}, '-_id -__v', {sort: {'_id': -1}});

    if (newTauxCO2.length !== null) {
        socketIO.emit("tauxCO2HubNew", newTauxCO2);
    }
}

exports.tauxCO2lEmitLast = tauxCO2lEmitLast;


//Fonction d'emission de toutes les temperatures socketio :
async function tauxCO2EmitAll(socketIO) {
    console.log("emet tous les taux de CO2");
    var tauxCO2lAll = await TauxCO2.find({}, '-_id -__v', {sort : {'date' : 1}});

    if (tauxCO2lAll.length !== null) {
        socketIO.emit("tauxCO2lAllHub", tauxCO2lAll);
    }
}

exports.tauxCO2EmitAll = tauxCO2EmitAll;

// Fonction pour récup et envoyer la consigne
const consigneTauxCO2 = (req, res, SocketIO) =>{


    ConsigneTauxCO2.deleteMany().then(
        ()=>{
            let consignetauxCO2 = new ConsigneTauxCO2(req.query);
            // console.log(consignetauxCO2);
            consignetauxCO2.save();
            consigneTauxCO2EmitAll(SocketIO);
            res.status(200).json();
        }).catch((error) => {
        res.status(200).json();
    })



};
exports.consigneTauxCO2=consigneTauxCO2;

//Fonction d'emission de la temp de consigne :
async function consigneTauxCO2EmitAll(socketIO) {

    // console.log("emet le taux de co2 de consigne");
    // Varibale de transit afinn d'attendre la fin deu delete many/save
    var transit = await ConsigneTauxCO2.findOne({});

    await ConsigneTauxCO2.findOne({}).then(
        (consigneTauxCO2)=>{
            if (consigneTauxCO2.length !== null) {
                socketIO.emit("consigneTauxCO2Hub", consigneTauxCO2);
            }
        }
    )



}
exports.consigneTauxCO2EmitAll =consigneTauxCO2EmitAll;

