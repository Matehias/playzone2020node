const PH = require('../Models/PH');

const getAllPH = (req, res) =>{

    PH.find({}, "-_id -__v")
        .then(data => res.status(200).json(data))
        .catch(error => res.status(400).json(error));

};

exports.getAllPH = getAllPH;

const postPH = (box, value, res, SocketIO) => {


    try {
        // création d'une variable (objet) de type schéma Mongoose PH à partir du query (dans l'URL) de la requete
        let pH = new PH();
        pH.box = box;
        pH.value = value;
        pH.date = Date.now();
        console.log(pH);

        // Sauvegarde en db de l'objet contenu dans PH
        pH.save();
        pHEmitAll(SocketIO);
        pHEmitLast(SocketIO);
        // Status http + affichage d'un message :
        // res.status(200).json("Enregistrement en db réussi !");
    } catch (e) {
        console.log("L'enregistrement du message a échoué");
        res.status(500).json(e);

    }
};

exports.postPH = postPH;

//Fonction d'emission d'un nouveau PH socketio :
async function pHEmitLast(socketIO) {
    console.log("emet un nouveau PH");
    var newPH = await PH.findOne({}, '-_id -__v', {sort: {'_id': -1}});

    if (newPH.length !== null) {
        socketIO.emit("pHHubNew", newPH);
    }
}

exports.pHEmitLast = pHEmitLast;


//Fonction d'emission de tous les PH socketio :
async function pHEmitAll(socketIO) {
    console.log("emet tous les PH");
    var pHAll = await PH.find({}, '-_id -__v', {sort : {'date' : 1}});

    if (pHAll.length !== null) {
        socketIO.emit("pHHub", pHAll);
    }
}

exports.pHEmitAll = pHEmitAll;
