const TempAir = require('../Models/tempAir');
const ConsigneTemp = require('../Models/consigneTemp')

const getAllTempAir = (req, res) =>{

    TempAir.find({}, "-_id -__v")
        .then(data => res.status(200).json(data))
        .catch(error => res.status(400).json(error));

};

exports.getAllTempAir = getAllTempAir;

const postTempAir = (box, value, res, SocketIO) => {


    try {
        // création d'une variable (objet) de type schéma Mongoose TempAir à partir du query (dans l'URL) de la requete
        let tempAir = new TempAir();
        tempAir.box=box;
        tempAir.value=value;
        tempAir.date = Date.now();
        console.log(tempAir);

        // Sauvegarde en db de l'objet contenu dans tempAir
        tempAir.save();
        tempAirEmitAll(SocketIO);
        tempAirlEmitLast(SocketIO);
        // Status http + affichage d'un message :
        // res.status(200).json("Enregistrement en db réussi !");
    } catch (e) {
        console.log("L'enregistrement du message a échoué");
        res.status(500).json(e);

    }
};

exports.postTempAir = postTempAir;

//Fonction d'emission d'une nouvelle temp air socketio :
async function tempAirlEmitLast(socketIO) {
    console.log("emet une nouvelle temperature air");
    var newTempAir = await TempAir.findOne({}, '-_id -__v', {sort: {'_id': -1}});

    if (newTempAir.length !== null) {
        socketIO.emit("TempAirHubNew", newTempAir);
    }
}

exports.tempAirlEmitLast = tempAirlEmitLast;


//Fonction d'emission de toutes les temperatures socketio :
async function tempAirEmitAll(socketIO) {
    console.log("emet toutes les temperatures air");
    var tempAirlAll = await TempAir.find({}, '-_id -__v', {sort : {'date' : 1}});

    if (tempAirlAll.length !== null) {
        socketIO.emit("tempAirlAllHub", tempAirlAll);
    }
}

exports.tempAirEmitAll = tempAirEmitAll;

// Fonction pour récup et envoyer la consigne
const consigneTempAir = (req, res, SocketIO) =>{


        ConsigneTemp.deleteMany().then(
            ()=>{
                let ConsignetempAir = new ConsigneTemp(req.query);
                // console.log(ConsignetempAir);
                ConsignetempAir.save();
                consigneTempAirEmitAll(SocketIO);
                res.status(200).json();
            }).catch((error) => {
                res.status(200).json();
            })


    
};
exports.consigneTempAir=consigneTempAir;

//Fonction d'emission de la temp de consigne :
async function consigneTempAirEmitAll(socketIO) {

    // console.log("emet la temp de consigne");
    // Varibale de transit afinn d'attendre la fin deu delete many/save
    var transit = await ConsigneTemp.findOne({});

   let consigneTemp = await ConsigneTemp.findOne({})


    if (consigneTemp.length !== null) {
        socketIO.emit("consigneTempHub", consigneTemp);
    }
}
exports.consigneTempAirEmitAll =consigneTempAirEmitAll;

