// Template créé à la création du projet avec IntelliJ :
//
// var createError = require('http-errors');
// var express = require('express');
// var path = require('path');
// var cookieParser = require('cookie-parser');
// var logger = require('morgan');
//
// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');
//
// var app = express();
//
// // view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'pug');
//
// app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));
//
// app.use('/', indexRouter);
// app.use('/users', usersRouter);
//
// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });
//
// // error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};
//
//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });
//
// module.exports = app;


const express = require('express');
const mongoose = require('mongoose');
const request = require('request');
const bodyParser = require('body-parser');


const app = express();
app.use(express.json());


const portListen = 3000;

// Création du serveur :
const httpApi = require("http").createServer(app);
// Création de Socket.io :
const socketIO = require("socket.io")(httpApi);

// Ajout (instanciation ?) du modèle (schéma) Mongoose :
const HumiditeSol = require('./Models/humiditeSol');


let _HumidSolController = require('./Controllers/humidsolController');
let _TempAirController = require('./Controllers/tempAirController');
let _HumidAirController = require('./Controllers/humidAirController');
let _TauxCO2Controller = require('./Controllers/tauxCO2Controller');
let _PHController = require('./Controllers/pHController');

// Connection db :
mongoose.connect('mongodb://localhost:27017/playzone',
    {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => {
        console.log("Connexion à MongoDB réussie sur le port 27017")
    })
    .catch(() => {
        console.log("Connexion à MongoDB ratée")
    });


//gestion des CORS
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS");
    next();
});

app.use(bodyParser.json());

// Initialisation de Socket.io :

socketIO.on('connection',
    (socket) => {
        console.log("Client connecté");

        if(socket.handshake.query.tutu == "HumiSolHubConnect"){
            _HumidSolController.humisolEmitAll(socketIO);
            _HumidSolController.humisolEmitLast(socketIO);
            _HumidSolController.consigneHumisSolEmitAll(socketIO);
        }

        if (socket.handshake.query.tutu == "TempAirHubConnect") {
            _TempAirController.tempAirlEmitLast(socketIO);
            _TempAirController.tempAirEmitAll(socketIO);
            _TempAirController.consigneTempAirEmitAll(socketIO);
        }

        if (socket.handshake.query.tutu == "HumiAirHubConnect") {
            _HumidAirController.humidAirlEmitLast(socketIO);
            _HumidAirController.humidAirEmitAll(socketIO);
            _HumidAirController.consigneHumidAirEmitAll(socketIO);
        }

        if(socket.handshake.query.tutu == "tauxCO2HubConnect"){
            _TauxCO2Controller.tauxCO2lEmitLast(socketIO);
            _TauxCO2Controller.tauxCO2EmitAll(socketIO);
            _TauxCO2Controller.consigneTauxCO2EmitAll(socketIO);
        }

        if (socket.handshake.query.tutu == "pHHubConnect") {
            _PHController.pHEmitAll(socketIO);
            _PHController.pHEmitLast(socketIO);
        }

        socket.on("disconnect", () => console.log("Client deconnecté"));
    }
);

// fonction de test :
// app.get("/", function(req, res,next)
// {
//     console.log("yun")
//     res.status(200).json()
// })

//**********************************************************************

// Récupérer les valeurs d'humidité du sol :
app.get("/api/humisol", function (req, res, next) {
    _HumidSolController.getAllHumidSol(req, res);
});


//Récupérer et envoyer la consigne d'humidité du sol :
app.get("/api/humidSolConsigne", function (req, res, next) {
    _HumidSolController.consigneHumiSol(req,res, socketIO)
})

// Récupérer une valeur humidité du sol :
//
// app.get("/api/humisol1", function (req, res, next) {
//
//     HumiditeSol.findOne(req.params, "-_id -__v")
//         .then(data => res.status(200).json(data))
//         .catch(error => res.status(400).json(error));
//
//
// });
// Enregistrer une donnée d'humidité du sol (pour tester et peupler la db, inutile à terme):
// app.post("/api/humisol", function (req, res, next) {
//
//
//     // deletaMany : supprime tout avant l'envoi d'une nouvelle donnée en db
//     HumiditeSol.deleteMany()
//         .then(
//             () => {
//                 // création d'une variable (objet) de type schéma Mongoose HumiditeSol
//                 let humiditeSol = new HumiditeSol(req.body);
//                 console.log(humiditeSol);
//                 // Sauvegarde en db de l'objet contenu dans humiditeSol
//                 humiditeSol.save();
//                 // Status http + affichage d'un message :
//                 res.status(200).json("Enregistrement en db réussi en effaçant la collection avant !");
//             }
//         )
//         .catch(
//             (e) => {
//                 res.status(400).send(e)
//             }
//         )
//
// });

//**********************************************************************
// Récupérer les valeurs de température de l'air :
app.get("/api/tempair", function (req, res, next) {
    _TempAirController.getAllTempAir(req, res);
});

//Récupérer et envoyer la consigne de temperature :
app.get("/api/tempConsigne", function (req, res, next) {
    _TempAirController.consigneTempAir(req,res, socketIO)
})




//*************************************************************
//Récupérer les valeur d'humidité de l'air
app.get("/api/humidair", function (req, res, next) {
    _HumidAirController.getAllHumidAir(req,res, socketIO);
});

//Récupérer et envoyer la cosigne d'humidté de l'air:
app.get("/api/humidAirConsigne", function (req, res, next) {
    _HumidAirController.consigneHumidAir(req,res, socketIO)
});

//*******************************************************************

//Récupérer les valeur de taux co2
app.get("/api/tauxco2", function (req, res, next) {
    _TauxCO2Controller.getAllTauxCO2(req,res, socketIO);
});

//Récupérer et envoyer la consigne du taux de co2:
app.get("/api/tauxco2Consigne", function (req, res, next) {
    _TauxCO2Controller.consigneTauxCO2(req,res, socketIO)
});


//*******************************************************************

//Récupérer les valeur de pH
app.get("/api/tauxco2", function (req, res, next) {
    _PHController.getAllPH(req,res, socketIO);
});

//********************************************************************

//  POST Enregistrer les données de tous les capteurs :
//attend un appel de type : http://localhost:3000/api/post/data?plantesol=Yun&valuesol=42&box=yunbox&value=42&tempair=42&tauxco2=42&ph=42
app.get("/api/post/data", function (req, res, next) {

    console.log("Réception des données");
    console.log(req.query)
    // res.status(200).json();
    _PHController.postPH(req.query.box, req.query.ph, res, socketIO);
    _TauxCO2Controller.postTauxCO2(req.query.box, req.query.tauxco2, res, socketIO);
    _HumidAirController.postHumidAir(req.query.box, req.query.value, res, socketIO);
    _HumidSolController.posHumidSol(req.query.plantesol, req.query.valuesol, res, socketIO);
    _TempAirController.postTempAir(req.query.box, req.query.tempair, res, socketIO);

    res.status(200).json("Enregistrement en db réussi !");
});


//*******************************************************************



/*
Partie YUN mode !

*/

//pour appeler directement l'arduino en mode controle par url on va faire
//prendra deux parametre, un la pin concernée et l'autre sont état 1 ou 0, LOW ou HIGH en fait
app.get("/api/arduino/digitalWrite", function(req, res, next){
    try
    {
        console.log(req.query)
        request("http://192.168.0.42/arduino/digital/"+req.query.pin+"/"+req.query.status, { json: true }, (err, res, body) =>
        {
            //console.log(body)
        });

        res.status(200).json();
    }
    catch (err){ res.status(400).send(err); }
});
// Appel vers le yun pour lui changer une variable (ici la consigne de température)
app.get("/api/arduino/seuilTemp", function(req, res, next){
    try
    {
        console.log(req.query)
        request("http://192.168.0.42/arduino/seuilTemp/"+req.query.value, { json: true }, (err, res, body) =>
        {
            console.log(body)
        });

        res.status(200).json(body);
    }
    catch (err){ res.status(400).send(err); }
});

// Appel vers le yun pour lui changer une variable (ici la consigne de d'humidité du sol)
app.get("/api/arduino/seuilHumiSol", function(req, res, next){
    try
    {
        console.log(req.query)
        request("http://192.168.0.42/arduino/seuilHumiSol/"+req.query.value, { json: true }, (err, res, body) =>
        {
            console.log(body)
        });

        res.status(200).json(body);
    }
    catch (err){ res.status(400).send(err); }
});

// Appel vers le yun pour lui changer une variable (ici la consigne de d'humidité de l'air)
app.get("/api/arduino/seuilHumiAir", function(req, res, next){
    try
    {
        console.log(req.query)
        request("http://192.168.0.42/arduino/seuilHumiAir/"+req.query.value, { json: true }, (err, res, body) =>
        {
            console.log(body)
        });

        res.status(200).json(body);
    }
    catch (err){ res.status(400).send(err); }
});

//Appel vers le yun pour lui changer une variable (ici la consigne du taux de co2)
app.get("/api/arduino/seuilTauxCO2", function(req, res, next){
    try
    {
        console.log(req.query)
        request("http://192.168.0.42/arduino/seuilTauxCO2/"+req.query.value, { json: true }, (err, res, body) =>
        {
            console.log(body)
        });

        res.status(200).json(body);
    }
    catch (err){ res.status(400).send(err); }
});

/*
 Fin
*/


httpApi.listen(portListen,
    () => {
        console.log(`Serveur dispo sur le port ${portListen}`)
    }
);


